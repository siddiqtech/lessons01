import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-facts',
  templateUrl: './facts.page.html',
  styleUrls: ['./facts.page.scss'],
})
export class FactsPage implements OnInit {
  public facts: any;

  constructor(private lessons: DataService) { }

  ngOnInit() {
    this.lessons.getLessonMenu('Actual Facts')
    .then(res => {
      return res;
    })
    .then(data => {
      this.facts = data
      console.log(this.facts)
    })
  }

}
