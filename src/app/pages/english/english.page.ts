import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-english',
  templateUrl: './english.page.html',
  styleUrls: ['./english.page.scss'],
})
export class EnglishPage implements OnInit {
  public english: any;

  constructor(private lessons: DataService) { }

  ngOnInit() {
    this.lessons.getLessonMenu('English Lesson C No. 1')
    .then(res => {
      return res;
    })
    .then(data => {
      this.english = data
      console.log(this.english)
    })
  }

}
