import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import * as _ from 'lodash';

@Component({
  selector: 'app-lessondetail',
  templateUrl: './lessondetail.component.html',
  styleUrls: ['./lessondetail.component.scss'],
})
export class LessondetailComponent implements OnInit {
  lesson: any;
  problems: boolean = false;

  constructor(private viewCtrl: ModalController) { }

  ngOnInit() {
    console.log(this.lesson)
    if (this.lesson.lessonName === 'The Problem Book') {
      this.problems = true;
    }
  }

  closeModel(){
    this.viewCtrl.dismiss()
  }

}

