import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LessonsPage } from './lessons.page';
import { LessondetailComponent } from './lessondetail/lessondetail.component';

const routes: Routes = [
  {
    path: '',
    component: LessonsPage
  },
  {
    path: ':id',
    component: LessondetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LessonsPageRoutingModule {}
