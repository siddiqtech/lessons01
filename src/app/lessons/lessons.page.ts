import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { LessondetailComponent } from './lessondetail/lessondetail.component';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-lessons',
  templateUrl: './lessons.page.html',
  styleUrls: ['./lessons.page.scss'],
})
export class LessonsPage implements OnInit {
  lessonShow = {};
  problems: boolean = false;
  facts: boolean = false;

  constructor(private lessons: DataService,
    public modal: ModalController) { }

  ngOnInit() {
    this.lessonShow = this.lessons.lessonShow;
    if (this.lessonShow[0].lessonName === 'The Problem Book') {
      this.problems = true;
    }
    if (this.lessonShow[0].lessonName === 'Actual Facts') {
      this.facts = true;
    }
    console.log(this.lessonShow);
  }

  async presentLessonDetail(lesson?: any){
    const modal = await this.modal.create({
      component: LessondetailComponent,
      componentProps: { lesson }
    });
    return await modal.present();
  }

}
