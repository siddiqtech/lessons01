import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  public menuData: any;

  constructor(public auth: AuthService, private lessons: DataService) { }

  ngOnInit() {
    let data = this.lessons.getMenu()
      .then(data => {
        return data;
      })
      .then(res => {
        this.menuData = res;
        console.log(this.menuData[0].lessonName);
      });
  }

}
