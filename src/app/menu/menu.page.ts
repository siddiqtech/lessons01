import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

import { LessonsPage } from '../lessons/lessons.page';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {

  public menuData: any;

  constructor(public auth: AuthService, private lessons: DataService, private router: Router, private navCtrl: NavController) { }

  ngOnInit() {
    let data = this.lessons.getMenu()
      .then(data => {
        return data;
      })
      .then(res => {
        this.menuData = res;
      });
  }

  handleBookClick(book: string) {
    const myURL = 'lessons/'+book;
    console.log(myURL);
    // this.router.navigate(['lessons'], { queryParams: {book} });
    let data = this.lessons.getLessonMenu(book).then(res => {
      return res;
    }).then(data => {
      this.router.navigateByUrl(myURL);
    })
  }

  handleFactsClick(){
    this.router.navigateByUrl('facts');
  }

  handleEnglishClick(){
    this.router.navigateByUrl('english');
  }

}
