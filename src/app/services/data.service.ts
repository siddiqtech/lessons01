import { Injectable } from '@angular/core';
import { resolve } from 'url';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  menuURL = 'http://www.kemmuhammad.com/api2/lessonsmenu';  
  lessonsMenu = {};
  lessonShow = {};

  constructor() { }

  async getMenu () {
    // const res = await fetch(this.menuURL);
    const res = await new Promise(resolve => {
      resolve(
        fetch(this.menuURL)
          .then(data => {
            let sendData = data.json();
            return sendData;
          })
          .then(res => {
            let retData = Object.values(res);
            return retData;
          })
      )
    });
    this.lessonsMenu = res;
    return res;
  }

  async getLessonMenu (id) {
    const res = await new Promise(resolve => {
      resolve(
        fetch(this.menuURL+'/'+id)
          .then(data => {
            let sendData = data.json();
            return sendData;
          })
          .then(res => {
            let retData = Object.values(res);
            return retData;
          })
      )
    });
    this.lessonShow = res;
    return res;
  }

  async getLessonDetail (id) {
    const res = await new Promise(resolve => {
      resolve(
        fetch(this.menuURL+'/lesson/'+id)
          .then(data => {
            let sendData = data.json();
            return sendData;
          })
          .then(res => {
            let retData = Object.values(res);
            return retData;
          })
      )
    });
    this.lessonShow = res;
    return res;
  }
}
